const express = require('express')
const path = require("path")
const fs = require('fs')
const csv = require('csvtojson')

const macthesPlayedPerYear = require('./src/server/1-matches-per-year')
const matchesWonPerTeamPerYear = require('./src/server/2-matches-won-per-team-per-year')
const extraRunsConceededPerTeamIN2016 = require('./src/server/3-extra-runs-conceded-per-team-in-2016')
const topTenEconomicalBowlersIn2015 = require('./src/server/4-top-10-economical-bowlers-in-2016')
const teamWonBothTossAndMatch = require('./src/server/5-no-of-times-each-team-won-toss-and-also-match')
const playerWithHighestPlayerOfTheMatchPerSeason = require('./src/server/6-player-with-highest-number-of-player-of-match-awards-in-each-season')
const bowlerWithBestEconomyInSuperOvers = require('./src/server/9-bowler-with-best-economy-in-super-overs')
const strikeRateOfBatsmanInEachSeason = require('./src/server/7-strike-rate-of-batsman-for-season')
const playersDismissedByAnotherPlayer = require('./src/server/8-highest-number-of-times-one-player-dismissed-by-another-player')


function main() {
    csv()
        .fromFile('./data/deliveries.csv')
        .then((deliveries)=> {
            csv()
                .fromFile('./data/matches.csv')
                .then(matches=>{
                    //const resultOfFirstQuestion = macthesPlayedPerYear(matches)
                    //saveMatchesPlayedPerYearToJsonFile(resultOfFirstQuestion)

                    //const resultOfSecondQuestion = matchesWonPerTeamPerYear(matches)
                    //saveMatchesWonPerTeamPerYearToJsonFile(resultOfSecondQuestion)

                    //const resultOfThirdQuestion = extraRunsConceededPerTeamIN2016(deliveries,matches,year="2016")
                    //saveExtraRunsConceededPerTeamIn2016ToJson(resultOfThirdQuestion)

                    //const resultOfFourthQuestion = topTenEconomicalBowlersIn2015(deliveries,matches,year="2015")
                    //saveTopTenEconomicalBowlersToJson(resultOfFourthQuestion)
                    
                    //const resultOfFithQuestion = teamWonBothTossAndMatch(matches)
                    //saveTeamWhichWonBothTossAndMatchToJson(resultOfFithQuestion)

                    //const resultOfSixQuestion = playerWithHighestPlayerOfTheMatchPerSeason(matches)
                    //savePlayerWithHighestPlayerOfTheMatchPerSeason(resultOfSixQuestion)

                    //const resultOfSeventhQuestion = strikeRateOfBatsmanInEachSeason(deliveries,matches)
                    //saveStrikeRateOfBatsmanInEachSeason(resultOfSeventhQuestion)
                    
                    //const resultOfEightQuestion = playersDismissedByAnotherPlayer(deliveries)
                    //saveHighestNumberOfPlayersDismissedByAnotherPlayer(resultOfEightQuestion)

                    const resultOfNinthQuestion = bowlerWithBestEconomyInSuperOvers(deliveries)
                    saveBowlerWithBestEconomyInSuperOvers(resultOfNinthQuestion)
                })
    })
}
main()


function saveMatchesPlayedPerYearToJsonFile(matchesPlayed) {
    fs.writeFile('./output/matchesPlayedPerYear.json', JSON.stringify({matchesPerSeason : matchesPlayed}), (error) => {
        if (error) {
            console.log(error)
        }
})}

function saveMatchesWonPerTeamPerYearToJsonFile(matchesWon) {
    fs.writeFile('./output/matchesWonPerTeamPerYear.json', JSON.stringify({matchesWonPerSeasonPerTeam : matchesWon}), (error) => {
        if (error) {
            console.log(error)
        }
})}

function saveExtraRunsConceededPerTeamIn2016ToJson(extraRunsData) {
    fs.writeFile('./output/extraRunsConceededPerTeamIn2016.json', JSON.stringify({extraRunsConceededPerTeamIN2016 : extraRunsData}), (error) => {
        if (error) {
            console.log(error)
        }
})}

function saveTopTenEconomicalBowlersToJson(topTenEconomyBolwers) {
    fs.writeFile('./output/topTenEconomicalBowlerIn2015.json', JSON.stringify({topTenEconominalBowlersIn2015 : topTenEconomyBolwers}), (error) => {
        if (error) {
            console.log(error)
        }
})}

function saveTeamWhichWonBothTossAndMatchToJson(teamWhichWonBothTossAndMatch) {
    fs.writeFile('./output/numberOfTimesTeamWhichWonBothTossAndMatch.json', JSON.stringify({teamWhichWonBothTeamAndatch : teamWhichWonBothTossAndMatch}), (error) => {
        if (error) {
            console.log(error)
        }
})}

function savePlayerWithHighestPlayerOfTheMatchPerSeason(playerWithHighestNumberOfPlayerOfTheMatchPerSeason) {
    fs.writeFile('./output/playerWithHighestNumberOfPlayerOfTheMatchPerSeason.json', JSON.stringify({playerWithHighestNumberOfPlayerOfTheMatchPerSeason : playerWithHighestNumberOfPlayerOfTheMatchPerSeason}), (error) => {
        if (error) {
            console.log(error)
        }
})}

function saveBowlerWithBestEconomyInSuperOvers(bestEcomomyBowler) {
    fs.writeFile('./output/bestEconomyBowlerInSuperOvers.json', JSON.stringify({BowlerWithBestEconomyInSuperOvers : bestEcomomyBowler}), (error) => {
        if (error) {
            console.log(error)
        }
})}

function saveStrikeRateOfBatsmanInEachSeason(strikeRateOfBatsman) {
    fs.writeFile('./output/strikeRateOfBatsmanInEachSeason.json', JSON.stringify({StrikeRateOfBatsmanInEachSeason : strikeRateOfBatsman}), (error) => {
        if (error) {
            console.log(error)
        }
})}

function saveHighestNumberOfPlayersDismissedByAnotherPlayer(resultOfEightQuestion) {
    fs.writeFile('./output/highestNumberOfTimesPlayerDismissedByAnotherPlayer.json', JSON.stringify({HighestNumberOfTimesPlayerDismissedByAnotherPlayer : resultOfEightQuestion}), (error) => {
        if (error) {
            console.log(error)
        }
})}

const app = express()

app.use(express.static(path.join(__dirname,"public")))

const PORT = process.env.PORT || 3000

app.listen(PORT, ()=> console.log(`Server started listening on http://localhost:${PORT}`))
