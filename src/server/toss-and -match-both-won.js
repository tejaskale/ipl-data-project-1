const fs = require("fs");
const path = require("path");
const csv= require("csv-parser");

function wonTossAndMatch() {
    let matches = [];
    fs.createReadStream('../data/matches.csv').pipe(csv({ })).on('data', function (datarow) {
        matches.push(datarow);
    })
        .on("end", () => {
            wonTossAndMatch = matches.reduce((accu,match)=>{
                if (match.toss_winner === match.winner) {
                    if (match.toss_winner in accu) {
                        accu[match.toss_winner] += 1;
                    } else {
                        accu[match.toss_winner] = 1;
                    }
                }
                return accu;
            },{})
            fs.writeFile(
                path.join(__dirname, "../public/output/won-toss-and-match.json"),
                JSON.stringify(wonTossAndMatch),
                (error) => {
                    if (error !== null) {
                        console.log(error);
                    }
                }
            )

        })
}

wonTossAndMatch();

