const fs = require("fs");
const csv = require("csv-parser");
const path = require("path");

function bestEconomicalBowlers() {
    let matches = [];
    fs.createReadStream('../data/deliveries.csv').pipe(csv({ })).on('data', function (datarowDeliveries) {

        if (datarowDeliveries.is_super_over === "1") {
            matches.push(datarowDeliveries);
        }
    })
        .on('end', () => {
            let bowlersList = matches.reduce((result,match)=>{
          
                if (result[match.bowler] === undefined) {
                    {result[match.bowler] = { totalRuns: 0, totalBalls: 0, }}
                }
                else{
                    {result[match.bowler].totalRuns += +match.total_runs;
                    result[match.bowler].totalBalls += 1;
                    result[match.bowler]["economy"] = +(result[match.bowler]["totalRuns"] / (result[match.bowler]["totalBalls"] / 6)).toFixed(2)};
                } 
                
                return result;
            },[])

            
            let bestBowler = Object.entries(bowlersList).sort(function ([firstPlayer, firstPlayerDetails], [secondPlayer, secondPlayerDetails]) {
                return firstPlayerDetails.economy - secondPlayerDetails.economy;
            })[0]
            console.log(bowlersList);


            fs.writeFile(
                path.join(__dirname, "../public/output/bowler-with-Best-economy-super-over.json"),
                JSON.stringify(bestBowler),
                (error) => {
                    if (error !== null) {
                        console.log(error);
                    }
                }
            )
        })
}

bestEconomicalBowlers();
