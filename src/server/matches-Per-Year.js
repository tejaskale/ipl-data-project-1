const fs = require("fs");
const path = require("path");
const csv = require("csv-parser");

function matchesPerYear() {
  let matches = [];
  fs.createReadStream("../data/matches.csv")
    .pipe(csv({}))
    .on("data", function (datarow) {
      matches.push(datarow);
    })
    .on("end", () => {
      let resultMatchesObject = matches.reduce((result, matches) => {
        // console.log(matches.season);
        if (matches.season in result) {
          result[matches.season] += 1;
        } else {
          result[matches.season] = 1;
        }
        return result;
      }, {});

      fs.writeFile(
        path.join(__dirname, "../public/output/matches-per-year.json"),
        JSON.stringify(resultMatchesObject),
        (error) => {
          if (error !== null) {
            console.log(error);
          }
        }
      );
    });
}

matchesPerYear();
