const fs = require("fs");
const csv = require("csv-parser");
const path = require("path");

function extraRunsConceded2016() {
    let matchStart;
    let matchEnd;
    fs.createReadStream(path.join(__dirname, '../data/matches.csv')).pipe(csv({ })).on('data', function (rowDataMatches) {
        if (rowDataMatches.season == 2016) {
            // console.log(rowDataMatches);
            if (matchStart === undefined) {
                matchStart = +rowDataMatches.id;
            } else {
                matchEnd = +rowDataMatches.id;
            }
        }
    }).on('end', () => {
        let matchesInSeason = [];
        fs.createReadStream(path.join(__dirname, '../data/deliveries.csv')).pipe(csv({ })).on('data', function (rowDataDeliveries) {
            // console.log(rowDataDeliveries);
            if (rowDataDeliveries.match_id >= matchStart && rowDataDeliveries.match_id <= matchEnd) {
                matchesInSeason.push(rowDataDeliveries);
            }

        }).on('end', () => {
            let extraRunsConceded = matchesInSeason.reduce((result,match)=>{
                
                if (result[match.bowling_team] !== undefined){
                    result[match.bowling_team] += +match.extra_runs;
                }else{
                    result[match.bowling_team] = +match.extra_runs;
                }
                return result
            },{})

            let teamNames= Object.keys(extraRunsConceded)
            let runs = Object.values(extraRunsConceded)

            console.log(teamNames);
            console.log(runs);
            
            fs.writeFile(
                path.join(__dirname, "../public/output/extra-runs-per-team-2016.json"),
                JSON.stringify(extraRunsConceded),
                (error) => {
                    if (error !== null) {
                        console.log(error);
                    }
                }
            )
        })
    })

}

extraRunsConceded2016();

