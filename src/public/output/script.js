//-----------------------------------Number of matches played per year-----------------------------------------------------------------------------------------------------------------------
  
document.addEventListener("DOMContentLoaded",() => {

    Highcharts.chart("numberOfMatchesPerYear", {
        chart: {
        type: 'area',
        zoomType: 'x',
        panning: true,
        panKey: 'shift',
        scrollablePlotArea: {
          minWidth: 600
        }
      },
        title : {
            text: "Number Of Matches Played Per Season"
        },
        xAxis: {
            title: {
                text: "Seasons"
            },
            labels: {
                style: {
                    fontSize:"16px",
                    color: "#000000"
                }
            },
            categories: [
                '2008', '2009',
                '2010', '2011',
                '2012', '2013',
                '2014', '2015',
                '2016', '2017'
              ]
        },
        yAxis: {
            title: {
                text: "matchesPlayerSeason"
            },
            labels: {
                style: {
                    fontSize:"16px",
                    color: "#000000"
                }
            },
        },
        series: [{
            data: [
                58, 57, 60, 73, 74,
                76, 60, 59, 60, 59
              ],
              dataLabels: {
                enabled: true,
                rotation: 0,
                color: '#FFFFFF',
                y: 20, // 10 pixels down from the top
                style: {
                    fontSize: '14px',
                    fontFamily: 'Verdana, sans-serif',
                }
            }
        }],
        
    
    })
    });
    
    
    // ----------------------------------------- < extra runs conceeded in 2016--> ---------------------------
    
    document.addEventListener("DOMContentLoaded",() => {
    
    
    Highcharts.chart("extrarunsIN2016", {
        chart : {
            type: "column",
        },
        title : {
            text: "extra runs conceeded per team in 2016"
        },
        xAxis: {
            title: {
                text: "TeamNames"
            },
            labels: {
                style: {
                    fontSize:"16px",
                    color: "#000000"
                }
            },
            categories: [
    'Rising Pune Supergiants',
    'Mumbai Indians',
    'Kolkata Knight Riders',
    'Delhi Daredevils',
    'Gujarat Lions',
    'Kings XI Punjab',
    'Sunrisers Hyderabad',
    'Royal Challengers Bangalore'
    ]
        },
        yAxis: {
            title: {
                text: "extraRunsConceededin2016"
            },
            labels: {
                style: {
                    fontSize:"16px",
                    color: "#000000"
                }
            },
        },
        series: [{
            data: [
    108, 102, 122,
    106,  98, 100,
    107, 156
    ],
              dataLabels: {
                enabled: true,
                rotation: 0,
                color: '#FFFFFF',
                y: 20, // 10 pixels down from the top
                style: {
                    fontSize: '14px',
                    fontFamily: 'Verdana, sans-serif',
                },
            }   
        }],
    })
    })
    
    
    //------------------------------------Top economical bowlers 2015-----------------------------------------------------------------------------------------------------------------------
    
    document.addEventListener("DOMContentLoaded", async()=>{
        const data = await fetch("./top-economical-bowlers.json")
        .then((response)=> response.json())
        .then(data=>{
            return data
        });
        const bowlers = data.map((eachObject) => Object.keys(eachObject)).flat()
        const rawEconomy = data.map((current)=>{
            return Object.values(current)
        })
        const flatData = rawEconomy.flat()
        const economy = flatData.map((current)=>{
            return current.economy
        })
        Highcharts.chart('EconomyBowlersIn2015', {
          title:{
            text:"Economical Bowlers in 2015",
          },
    
          chart:{
            zoomType: 'x'
          },
          title: {
            text: 'Economical Bowlers in 2015'
          },
          xAxis: {
            title:{
              text:"BowlerNames",
              style:{
                fontSize:"16px",
                color:"Black",
              }
            },
            labels: {
                style: {
                    fontSize:"12px",
                    color: "black",
                }
            },
            categories: bowlers
          },
    
          yAxis:{
            title:{
                text:"Economy",
                style:{
                    fontSize:"16px",
                    color:"Black",
                  },
            },
            labels: {
                style: {
                    fontSize:"16px",
                    color: "black",
                }
            },
          },
    
          series:[
            {
                data:economy,
                dataLabels:{
                    enabled:true,
                    y: 20, // 10 pixels down from the top,    
                    style:{
                        fontSize:"12px"
                    }        
            }
        }
          ]
        });
    });
    
    
    // ------------------------------------------------------- <!-- highest-PlayerOfTheMatch-awards--> -------------------------------------------
    
    
    document.addEventListener("DOMContentLoaded",async () => {
       const data= await fetch('./highest-PlayerOfTheMatch-awards.json')
            .then((response) => response.json())
            .then((data) => {
                return data
            })
            const players = Object.values(data);
            const playersName = players.map(current=>current[0].name);
            const awards = players.map(current=>current[0].playerOfTheMatchCount);
            // console.log(playersName);
            Highcharts.chart("highestAwards", {
                chart : {
                    type: "column",
                },
                title : {
                    text: "Highest-PlayerOfTheMatch-awards"
                },
                xAxis: {
                    title: {
                        text: "PlayersNames"
                    },
                    labels: {
                        style: {
                            fontSize:"16px",
                            color: "#000000"
                        }
                    },
                    categories: playersName
                },
                yAxis: {
                    title: {
                        text: "awards"
                    },
                    labels: {
                        style: {
                            fontSize:"16px",
                            color: "#000000"
                        }
                    },
                },
                series: [{
                    data: awards,
                      dataLabels: {
                        enabled: true,
                        rotation: 0,
                        color: '#FFFFFF',
                        y: 20, // 10 pixels down from the top
                        style: {
                            fontSize: '14px',
                            fontFamily: 'Verdana, sans-serif',
                        },
                    }
                }]
            })
    });
    
    
    // ------------------------------------------------------- <!-- Number of matches won per team per year--> ------------------------------------------
    
    document.addEventListener("DOMContentLoaded",() => {
        fetch('./matches-won-per-team-per-year.json')
            .then((response) => response.json())
            .then((data) => {
                matchesWonByTeamPerSeason(data)
            })
    
        function matchesWonByTeamPerSeason(data) {
            const seasons = [];
            const teamNames = [];
            for (let key in data) {
                seasons.push(key)
                for (let teamName in data[key]) {
                    if (!teamNames.includes(teamName)) {
                        teamNames.push(teamName);
                      }
                  }
              }
              const seasonsData = [];
              for (let teamName in teamNames) {
                  let arr = [];
                  for (let season in seasons) {
                      if (data[seasons[season]].hasOwnProperty(teamNames[teamName])) {
                          arr.push(data[seasons[season]][teamNames[teamName]]);
                      }
                      else {
                          arr.push(0);
                      }
                  }
                  let obj = {
                      'name': teamNames[teamName],
                      'data': arr
                  }
                  seasonsData.push(obj);
              }
    
              Highcharts.chart("matchesWonPerTeamPerYear", {
                chart: {
                    type: 'column'
                },
                title: {
                    text: "Number of matches won per team per year"
                },
                xAxis: {
                    categories: seasons,
                    crosshair: true
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: "Matches Won"
                    }
                },
                tooltip: {
                    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                    pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                        '<td style="padding:0"><b>{point.y:.1f} mm</b></td></tr>',
                    footerFormat: '</table>',
                    shared: true,
                    useHTML: true
                },
                plotOptions: {
                    column: {
                        pointPadding: 0.2,
                        borderWidth: 0
                    }
                },
                series: seasonsData
            });
        }
    });
    
    
    // ------------------------------------------------------- <!-- Strike Rate of a batsman Per year--> ------------------------------------------
    
    
    document.addEventListener("DOMContentLoaded",async () => {
        const data = await fetch('../output/strike-rate-of-a-batsman-per-season.json')
            .then((response) => response.json())
            .then((data) => {
                return data
            })
            const seasons = Object.keys(data);
            const rawData = Object.values(data);
            const batsmanData = rawData.map((current)=>{
                return current.strikeRate
            })
              Highcharts.chart("strikeRateOfaBatsman", {
                chart: {
                    type: 'line'
                },
                title: {
                    text: "Strikr rate of a batsman per season"
                },
                xAxis: {
                    categories: seasons,
                    crosshair: true
                },
                yAxis: {
                    title: {
                        text: "Strike Rate"
                    },
                    labels: {
                        style: {
                            fontSize:"16px",
                            color: "#000000"
                        }
                    },
                },
                series:[{
                    data: batsmanData,
                      dataLabels: {
                        enabled: true,
                        rotation: 0,
                        color: '#FFFFFF',
                        y: 20,
                        style: {
                            fontSize: '14px',
                            fontFamily: 'Verdana, sans-serif',
                        },
                    }
                }]
            });
    });
    
    
    // ----------------------------------------Toss winner is a match winner---------------------------------------------------------------------------------------------
    
    document.addEventListener("DOMContentLoaded", async () => {
      const data = await fetch('./won-toss-and-match.json')
      .then((response)=> response.json())
      .then(data=>{
          return data
      });
    //   console.log(data);
      const teams = Object.keys(data)
      const wins = Object.values(data)
    
      Highcharts.chart('matchWonByTossWinner', {
        title:{
          text:"Toss And Match Winners",
        },
    
        chart:{
          type:'column'
        },
    
        xAxis: {
          title:{
            text:"Teams",
            style:{
              fontSize:"16px",
              color:"Black",
            }
          },
          labels: {
              style: {
                  fontSize:"12px",
                  color: "black",
              }
          },
          categories: teams
        },
    
        yAxis:{
          title:{
              text:"Wins",
              style:{
                  fontSize:"16px",
                  color:"Black",
                },
          },
          labels: {
              style: {
                  fontSize:"16px",
                  color: "black",
              }
          },
        },
    
        series:[
          {
              data:wins,
              dataLabels:{
                  enabled:true,
                  y: 20,    
                  style:{
                      fontSize:"12px"
                  }        
          }
      }
        ]
      });
    });